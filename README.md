# README #

This README would normally document whatever steps are necessary to get your application up and running.

###For now, the idea is to build a script to:  
1. Generate a signed key  
2. Create the \*.connectedApp file 
2. Place the signed certificate in a Connected App  
3. Deploy the Metadata for the Conneceted App to a Salesforce Org  

###In order to do this we need to perform the following steps:  
1. Build a docker image based on Ubuntu with: 
 a. Node  
 b. NPM  
 c. DMC  
 d. SFDX CLI  
 e. Selenium (or equivalent)  
 f. Chrome Headless  

2. Build a bamboo plan to:  
 1. Checkout the repo  
 1. Mark script executable (chmod)  
 1. Execute Repo Generation Script which   
  * Generates Key File and Certificate  
  * Creates a git repository  
  * Initialize SFDX  
  * Creates the key folder  
  * Moves the key into the folder  
  * commit && push  
 1. Execute ConnectedApp Generation Script

3.) The ConnectedApp generation script has the following variables:  
 * Salesforce domain (login/test/custom)  
 * Username  
 * Password w/ Security Token (if needed)  
 * CA Information for \*.crt file  

4.) The Connected App Generation script itself will:  
Generate the \*.connectedApp XML doc  
Groom the contents of the server.crt file  
Insert the contents into the \*.connectedApp file  
DMC Deploy the Connected App  

5.)Selenium then:  
	a.) Open Salesforce  
	b.) Login  
	c.) Go to App Manager  
	d.) Go to JWT App  
	c.) Manage  
	d.) Edit Policies  
	e.) Allow Pre-auth users  
	f.) Add SysAdmin Profile to the Profiles List  
	
The script to generate the key is:
```bash
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
openssl rsa -passin pass:x -in server.pass.key -out server.key
openssl req -new -key server.key -out server.csr
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt #with parameters
```